'use strict';

const { networkInterfaces } = require('os');

const getInterfaces = () => {
    const nets = networkInterfaces();
    const results = {};
    
    for (const name of Object.keys(nets)) {
        for (const net of nets[name]) {
            // Skip over non-IPv4 and internal (i.e. 127.0.0.1) addresses
            if (net.family === 'IPv4' && !net.internal) {
                if (!results[name]) {
                    results[name] = "";
                }
                results[name] = net.address;
            }
        }
    }
    if(!('eth0' in results))
    {
       results['eth0'] = "";
    }
    if(!('wlan0' in results))
    {
        results['wlan0'] = "";
    }
    return results;
};

module.exports = { getInterfaces }