const LCD = require('raspberrypi-liquid-crystal');
class LCDHandler {

    constructor() {
        this.lcd = new LCD(2, 0x27, 16, 2);
        this.lcd.beginSync();
        this.lcd.clearSync();
    }
    setIpAddress(wlan, wifi)
    {
        this.lcd.clearSync();
        this.lcd.setCursorSync(0, 0);
        this.lcd.printSync('W:'+ wifi);
        this.lcd.setCursorSync(0, 1);
        this.lcd.printSync('L:'+ wlan);
    }
};


module.exports = LCDHandler