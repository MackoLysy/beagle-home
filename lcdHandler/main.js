
const LCDHandler = require("./src/lcd");
const Interfaces = require("./src/getInterfaces");

const lcd = new LCDHandler();

setInterval(() => {
    const result = Interfaces.getInterfaces();
    lcd.setIpAddress(result['eth0'], result['wlan0']);
}, 1000);



